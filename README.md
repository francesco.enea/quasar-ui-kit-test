<img src="https://img.shields.io/npm/v/quasar-ui-test.svg?label=quasar-ui-test">


Compatible with Quasar UI v2 and Vue 3.

# Structure
* [/ui](ui) - standalone npm package


# Donate
If you appreciate the work that went into this project, please consider [donating to Quasar](https://donate.quasar.dev).

# License
MIT (c) Francesco Enea <francesco@consonant.it>
