import { h } from 'vue'
import { QBadge } from 'quasar'

export default {
  name: 'TestButton',

  setup () {
    return () => h(QBadge, {
      class: 'TestButton',
      label: 'TestButton'
    })
  }
}
