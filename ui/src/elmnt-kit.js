import Component from './components/Component';
import EButton from './components/EButton.vue';

const version = __UI_VERSION__;

function install(app) {
  app.component(Component.name, Component);
  app.component(EButton.name, EButton);
}

export { version, Component, EButton, install };
